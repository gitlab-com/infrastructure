<!-- This issue is for requesting or reporting issues with accessing rails or db console through Teleport.

Provisioning of Teleport Access for the first time is the responsibility of IT Ops and should go through the [Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/) process.

The Production Engineering::Foundations team owns the underlying infrastructure supporting Teleport, and we can assist in debugging and trouble shooting issues with accessing db console or rails console once the baseline entitlements have been granted by IT and you have a Teleport Okta tile available to you.
-->


### This issue is with:

- [ ] database console access
- [ ] rails console access
- [ ] in staging
- [ ] in production
- [ ] unable to log in to Teleport's web UI


### Description of the issue

<!-- Please describe the problem connecting in as much detail as possible. If you have screen shots or videos capturing the problem, please include them here or in the comments below.
-->


### What, if anything, have you tried already to resolve the issue?

<!-- It can be helpful to know what was already tried but did not help so we don't suggest it again. -->


### Reporter Issue Checklist

* [ ] I have a Teleport tile visible when I'm logged in to Okta
* [ ] I have followed the setup instructions for the access I'm seeking
  * [ ] [Database console](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/teleport/Connect_to_Database_Console_via_Teleport.md)
  * [ ] [Rails console](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/teleport/Connect_to_Rails_Console_via_Teleport.md)
* [ ] Provide output of `tsh login --proxy production.teleport.gitlab.net` and `tsh status` along with any commands you are trying to execute

/label ~"team::Foundations", ~"workflow-infra::Triage", ~"unblocks others" ~"Foundations::Requests"
/epic &1530
