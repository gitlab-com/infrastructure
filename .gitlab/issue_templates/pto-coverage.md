## :map: Intent

This issue describes how broader group and team members are [facilitating a team member's role to take meaningful time away from work](https://about.gitlab.com/handbook/paid-time-off/#paid-time-off). The intent is that the team member isn't required to "catchup" before and after taking a well-deserved vacation and also to act as a reference if there are questions about work that you were involved with while you are away.

A handoff issue like this should be used anytime you take one week (5 days) or more of time off.

## :clipboard: Responsibilities

Include any regularly performed responsibilities. This could include things like triaging issues, leading meetings, responding to certain requests, rotation duties, etc...

| Priority | Project/Subject | Context Link | Primary | Secondary |
| -------- | --------------- | ------------ | ------- | --------- |
| HIGH     |                 |              |         |           |
| MEDIUM   |                 |              |         |           |

## :handshake: Coverage Tasks

Include specific issue and merge request links for tasks in progress or to be completed. Add any notes about the expected progress (current state should live within the issue/MR itself)

- [ ] Item - Link - `@assignee`

## :book: References

- [Foundations handbook page](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/foundations/)
- [Ops handbook page](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/ops/)

## :white_check_mark: Issue Tasks

### :o: Opening Tasks

- [ ] Assign to yourself
- [ ] Title the issue `PTO Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add a comment to the issue that says ":recycle: Retrospective Thread, What Did I Miss?", to collect feedback on what information you may have forgotten to document ahead of your PTO
- [ ] Assign due date for your last PTO day
- [ ] Add any relevant references including direction pages, group handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete or monitor and assignees
- [ ] Assign this issue to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your team, group, project, and any other relevant Slack channels
- [ ] Ensure you've assigned tasks via [Time Off by Deel](https://handbook.gitlab.com/handbook/people-group/paid-time-off/#roles-and-task-handoffs) including assigning relevant tasks to a slack channel 
- [ ] Ensure your Time Off by Deel auto-responder points team members to this issue
- [ ] Make sure your time off is tracked as OOO in your calendar, and your ModernLoop settings respect OOO so that no interviews are scheduled while you are out (you can access ModernLoop via the tile in Okta)
- [ ] Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#current-status) to include a reference to this issue. This issue is confidential, so your being on PTO will not be public, but it is helpful to also say "OOO" in your status.

### :x: Closing Tasks

- [ ] Review the [Returning from Time Off](https://about.gitlab.com/handbook/paid-time-off/#returning-from-pto) guidance
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/blob/main/.gitlab/issue_templates/pto-coverage.md)

/confidential

/labels ~"workflow-infra::Triage" ~"group::Production Engineering"

/assign me
