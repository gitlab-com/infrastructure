# Overview

{+ Service Name +} is an unowned service which is by default marked as `reliability_unowned`.

The absence of defined ownership increases the risk that the service will not be properly maintained, and in the event of an incident or performance degradation finding a team willing or knowledgeable enough to support it may cause a bigger impact to customers than if ownership was pre-defined.

Reference in service-catalog : {+Add link to the service in service-catalog+}

# Proposed Solution

Identification and assignment of service Owners and updating the service catalog to reflect the correct owner for each service, ensuring accountability.

# Steps

The following steps might help us direct to the right team that can own the service 

- [ ] Mention the team that has the most context : {+Team Name+}
- [ ] Mention the team that might be the rightful owner based on the service purpose : {+Team Name+}
- [ ] Once ownership has been agreed upon, update the service-catalog with the new owner