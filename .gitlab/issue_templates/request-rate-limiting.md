<!--
This template is for GitLab team members to request information or assistance for a user's rate limiting settings.

Note, general requests related to functionality [within the GitLab application](https://gitlab.com/gitlab-org/gitlab/), should be directed to the appropriate stage team using the standard [feature request template](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal).

Please fill out as many details as possible below.
-->

#### Customer Details

*this issue is confidential*

- **Customer IP Address(es) affected by rate limits**: [+ IP Information +]
- **Customer Name**: [+ Customer Name +]
- **Customer Plan**: [+ Free/Premium/Ultimate/Dedicated +]


#### Details of Customer's workflow being impacted by rate limits

*Describe how the user is using GitLab.com in a way to come up against rate limits. Detail specific APIs where possible*

[+ Workflow Details +]

**APIs being used in this workflow**: [+ APIs Used +]

#### Request for assistance

*Any additional details on what assistance is needed to support the customer*

Related [Rate Limiting Bypass Policy](https://handbook.gitlab.com/handbook/engineering/infrastructure/rate-limiting/bypass-policy)

<!--
please do not edit the below
-->

/labels ~"workflow-infra::Triage" ~"group::Production Engineering" ~"Support::Log Request" ~"team::Foundations" ~"Service::RateLimiting" ~"Foundations::Requests"
/confidential
/epic &1530
